# Noms des binômes
AOURIR Ikram
THUILLIER Laura

# Etapes réalisées dans le TP
1. Création du repo sur gitlab et récupération sur postes <AOURIR Ikram> <THUILLIER Laura>
2. Création du .gitignore
3. Création des packages
4. Création d'une branche dev
5. Création d’une première base du Shop (packages mode et controller, classes Item Storage et Shop), edition pom.xml <AOURIR Ikram> <THUILLIER Laura> sur le poste de <THUILLIER Laura>
6. Push de la première version de Shop 
7. Créations de nouvelles fonctionnalités
    * Création de la branche featureA <THUILLIER Laura>
    * Ajout d'une Launch et modifications Shop <THUILLIER Laura>
    * Push sur le serveur et merge avec dev <THUILLIER Laura>
    * Création de la branche featureB <AOURIR Ikram>
    * Modification de la classe Shop pour méthode buy et Launch <AOURIR Ikram>
    * Push sur le serveur et merge avec dev <AOURIR Ikram>
    * Résolution des conflits <AOURIR Ikram> <THUILLIER Laura> sur le poste de <AOURIR Ikram>
8. Créations de nouvelles classes
    * Création de la branche Consommable <AOURIR Ikram>
    * Développement des classes Consommable, Paper, Pen <AOURIR Ikram>
    * Push puis merge avec la branche master <AOURIR Ikram>
    * Création de la branche Book <THUILLIER Laura>
    * Développement de Book, BookToTouch, MusicalBook, PuzzleBook et OriginalBook <THUILLIER Laura>
    * Push sur le serveur et merge avec master <THUILLIER Laura>
9. Création des nouvelles fonctionnalités de shop 
    * Récupération du code en l'état (pull) <AOURIR Ikram> <THUILLIER Laura>
    * Création de la branche fonctions2_shop 
    * Développement des fonctionnalités isItemavailable, getAgeforBook, getAllBook, getNbItemStorage et getQuantityperConsumable <AOURIR Ikram> <THUILLIER Laura> de concert sur le poste <THUILLIER Laura>
    * Push et merge avec la branche master
10. Mise en place des tests unitaires pour toutes les fonctions des classes Shop et Storage
11. Push des tests unitaires
12. Edition finale du README.md
    


# Etapes non réalisées dans le TP
A priori, aucune.


# Liste des classes couvertes par des tests unitaires 
1. La classe Item (pour test au début du TP avec J. Saraydaryan)
2. Toutes les méthodes de Storage:
    * (test addItem() et getItem())
3. Toutes les méthodes de Shop
    * sell() et buy()
    * isItemAvailable(String name)
    * getAgeForBook(String name)
    * getINbtemInStorage(String name)
    * getQuantityPerConsumable(String name)
    * getAllBook()

Nous ne testons pas toutes les classes issues d'Item telles que Book, Consommable etc. car elles ne contiennent que des getters et setters.



# Commentaires