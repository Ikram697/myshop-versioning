
import com.ci.myShop.model.Book;
import com.ci.myShop.model.Consommable;
import com.ci.myShop.model.Item; 
import com.ci.myShop.controller.Shop; 
import com.ci.myShop.controller.Storage; 


public class Launch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		//Cr�ation de deux objets 
		Item item1 = new Item("Peluche",01,5,3) ;
		Item item2 = new Item("Camion en plastique",02,6,1) ;

		//Cr�ation de deux objets suppl�mentaires
		Item item3 = new Item("Poup�e",03,10,1) ;
		Item item4= new Item("Montre",04,20,1) ;


		//Cr�ation d'un Book
		Book book1 = new Book("Alice aux pays des merveilles",05,7,1);
		book1.setAge(10);

		Book book2 = new Book("L'Attrape-Coeur",06,7,1);
		book2.setAge(10);

		//Cr�ation d'un consommable
		Consommable conso1 = new Consommable("Cartons",07,2,10);
		conso1.setQuantity(100);

		//Ajout de trois objets dans un Storage
		Storage listeItems = new Storage();
		listeItems.addItem(item1) ;
		listeItems.addItem(item2) ;
		listeItems.addItem(item3) ;

		//ajout du livre
		listeItems.addItem(book1) ;
		listeItems.addItem(book2) ;

		//ajout du conso
		listeItems.addItem(conso1);

		//Initialisation du Shop avec le storage correspondant et du cash
		Shop newShop = new Shop() ;
		newShop.setStorage(listeItems);
		newShop.setCash(50);

		//V�rification de stock
		newShop.getINbtemInStorage(item1.getName());

		//V�rification nombre de conso
		newShop.getQuantityPerConsumable(conso1.getName());

		//Achat de l'objet non pr�sent dans le Shop par le Shop
		newShop.buy(item4);


		//Affichage du cash apr�s achat
		System.out.println(newShop.getCash()) ;

		//Realisation de la vente de cet objet nouvellement achet�
		newShop.isItemAvailable(item4.getName()); //verification pr�sence item4
		newShop.isItemAvailable("chocolat"); //v�rification pr�sence item non pr�sent
		newShop.sell(item4.getName());

		//Test fonctionnalit�s 2 du Shop
		newShop.getAgeForBook(item1.getName()); //objet non livre
		newShop.getAgeForBook(book1.getName());

		//Affichage du cash apr�s vente
		System.out.println(newShop.getCash()) ;

		//Affichage des livres dispos
		newShop.getAllBook();


	}
}

