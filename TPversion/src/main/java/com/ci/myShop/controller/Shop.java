/**
 * 
 */
package com.ci.myShop.controller;
import com.ci.myShop.model.Book;
import com.ci.myShop.model.Consommable;
import com.ci.myShop.model.Item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.ci.myShop.controller.Storage;
public class Shop {


	private Storage storage;
	private float cash;


	public float getCash() {
		return cash;
	}
	public void setCash(float cash) {
		this.cash = cash;
	}
	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}


	public Item sell(String name)
	{
		if ((storage.getItemMap()).containsKey(name))
		{

			Item objet_vendu = storage.getItemMap().get(name);

			storage.getItemMap().remove(objet_vendu.getName(), objet_vendu);

			System.out.println("Objet "+name+" vendu!");

			float prix_objet_vendu = objet_vendu.getPrice();
			this.cash += prix_objet_vendu ;


			return storage.getItemMap().get(name);
		}

		else
		{
			System.out.println("L'objet ne fait pas partie du magasin.");    
			return storage.getItemMap().get(name);
		}


	}
	public boolean buy(Item item)
	{

		float prix_objet = item.getPrice();

		if (this.getCash() >= prix_objet)
		{
			System.out.println("L'objet "+item.getName()+" a �t� ajout� � l'inventaire au prix de "+prix_objet+" euros.");
			storage.addItem(item);
			this.cash = this.cash - prix_objet ;
			return true;
		}

		else
		{
			System.out.println("Desole, le cash du magasin est insuffisant pour cet objet");
			return false;
		}

	}

	public boolean isItemAvailable(String name) {

		if ((storage.getItemMap()).containsKey(name))
		{
			System.out.println("L'item "+name+" est disponible");
			return true;
		}

		else
		{
			System.out.println("L'item "+name+" est indisponible");
			return false;
		}
	}



	public int getAgeForBook(String name) {

		Item objet = storage.getItem(name);
		if (objet instanceof Book)
		{
			Book book = (Book) objet;
			System.out.println("L'�ge minimum pour lire ce livre est "+book.getAge());
			return book.getAge();
		}

		else
		{
			System.out.println("Cet objet n'est pas un livre");
			return 0;
		}
	}

	public	int getINbtemInStorage(String name)
	{
		if ((storage.getItemMap()).containsKey(name))
		{

			System.out.println("Il y a "+(storage.getItem(name)).getNbrElt()+" objets "+name+" en stock.");
			return (storage.getItem(name)).getNbrElt();
		}
		else
		{
			System.out.println("L'objet n'est pas en stock.");
			return 0;
		}

	}

	public int getQuantityPerConsumable(String name)
	{
		Item objet = storage.getItem(name);
		if (objet instanceof Consommable)
		{
			Consommable conso = (Consommable) objet;
			System.out.println("Le consommable "+name+" dispose d'une quantit� de "+conso.getQuantity());
			return conso.getQuantity();
		}
		else
		{
			System.out.println("Cet objet n'est pas un consommable.");
			return 0;
		}

	}

	public List<Book> getAllBook()
	{

		List<Book> listOfBook = new ArrayList<Book>();
		List<String> listOfTitles = new ArrayList<String>();

		for (Map.Entry<String, Item> entry : storage.getItemMap().entrySet())
		{
			if (entry.getValue() instanceof Book)
			{
				listOfBook.add((Book) entry.getValue());
				listOfTitles.add(entry.getKey());
			}
			else
			{

			}
		}

		System.out.println(Arrays.toString(listOfTitles.toArray()));
		return listOfBook;
	}

}



