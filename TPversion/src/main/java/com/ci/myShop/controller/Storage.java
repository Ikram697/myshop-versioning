/**
 * 
 */
package com.ci.myShop.controller;

import java.util.HashMap;
import java.util.Map;

import com.ci.myShop.model.Item;


/**
 * @author Lou
 *
 */
public class Storage {
	
	private Map<String, Item> itemMap = new HashMap<>();

	public Map<String, Item> getItemMap() {
		return itemMap;
	}

	public void setItemMap(Map<String, Item> itemMap) {
		this.itemMap = itemMap;
	}
	
	
	public void addItem (Item obj)
	{
		this.itemMap.put(obj.getName(), obj);
	}

	public Item getItem(String name)
	{
		return this.itemMap.get(name);
		
	}

	
	

	
}
