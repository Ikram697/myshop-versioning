package com.ci.myShop.model;

public class Book extends Item {

	public Book(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}
	private int nbPage;
	private String author;
	private String publisher;
	private int year;
	private int age;
	
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return super.getName();
	}
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		super.setName(name);
	}
	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return super.getId();
	}
	@Override
	public void setId(int id) {
		// TODO Auto-generated method stub
		super.setId(id);
	}
	@Override
	public float getPrice() {
		// TODO Auto-generated method stub
		return super.getPrice();
	}
	@Override
	public void setPrice(float price) {
		// TODO Auto-generated method stub
		super.setPrice(price);
	}
	@Override
	public int getNbrElt() {
		// TODO Auto-generated method stub
		return super.getNbrElt();
	}
	@Override
	public void setNbrElt(int nbrElt) {
		// TODO Auto-generated method stub
		super.setNbrElt(nbrElt);
	}
	@Override
	public String display() {
		// TODO Auto-generated method stub
		return super.display();
	}
	public int getNbPage() {
		return nbPage;
	}
	public void setNbPage(int nbPage) {
		this.nbPage = nbPage;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
}
