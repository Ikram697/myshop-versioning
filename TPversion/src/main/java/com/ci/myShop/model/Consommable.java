/**
 * 
 */
package com.ci.myShop.model;
import com.ci.myShop.model.Item;
/**
 * @author ikramaourir
 *
 */

public class Consommable extends Item {
	
	public Consommable(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}

	private int quantity ;

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
