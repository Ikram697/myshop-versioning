/**
 * 
 */
package com.ci.myShop.model;

/**
 * @author Lou
 *
 */
public class Item {
	

	private String name;
	private int id;
	private float price;
	private int nbrElt;

	
	// Constructeur principal
	public Item (String name, int id, float price, int nbrElt) {
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
		
	}
	
	
//GET et SET
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getNbrElt() {
		return nbrElt;
	}
	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
	
	
	
	public String display()
	{
		return ("Nom de l'objet: "+name+"ID de l'objet: "+id+"Prix de l'objet: "+price);
	}

}
