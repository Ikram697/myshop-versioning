package com.ci.myShop.model;

import java.util.ArrayList;
import java.util.List;

public class MusicalBook extends Book {
	
	public MusicalBook(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}
	private List<String> listOfSound = new ArrayList<String>();
	private int lifetime;
	private int nbrBattery;
	
	
	
	public List<String> getListOfSound() {
		return listOfSound;
	}
	public void setListOfSound(List<String> listOfSound) {
		this.listOfSound = listOfSound;
	}
	public int getLifetime() {
		return lifetime;
	}
	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}
	public int getNbrBattery() {
		return nbrBattery;
	}
	public void setNbrBattery(int nbrBattery) {
		this.nbrBattery = nbrBattery;
	}

	
	
	
}
