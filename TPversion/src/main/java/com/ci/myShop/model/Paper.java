package com.ci.myShop.model;
import com.ci.myShop.model.Consommable;;

public class Paper extends Consommable {
	
	public Paper(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}
	private String quality ;
	private float weight;
	
	
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	

}
