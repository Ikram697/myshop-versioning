/**
 * 
 */
package com.ci.myShop.model;
import com.ci.myShop.model.Consommable;;
/**
 * @author ikramaourir
 *
 */
public class Pen extends Consommable{
	public Pen(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}
	private String color;
	private int durability ;
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getDurability() {
		return durability;
	}
	public void setDurability(int durability) {
		this.durability = durability;
	}
	
	
	
	

}
