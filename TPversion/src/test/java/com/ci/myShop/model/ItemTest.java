package com.ci.myShop.model;

import org.junit.Test;

public class ItemTest {

	@Test
	public void test_setName() {
		Item it =new Item("doe",01,5,1);
		it.setName("doe");

		assert("doe".equals(it.getName()));
	}

}
