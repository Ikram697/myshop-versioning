package com.ci.myShop.model;

import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Test;

import com.ci.myShop.controller.Shop;
import com.ci.myShop.controller.Storage;


public class ShopTest {

	@Test
	public void test_sell () {

		//On va tester la fonction de vente
		//On initialise d'abord des objets � vendre dans un stock

		Item item1 = new Item("Peluche",01,5,1) ;
		Item item2 = new Item("Camion en plastique",02,6,1) ;
		Storage listeItems = new Storage();
		listeItems.addItem(item1) ;
		listeItems.addItem(item2) ;

		//On initialise un magasin sans cash
		Shop newShop = new Shop() ;
		newShop.setStorage(listeItems);
		newShop.setCash(0);

		//On vend
		newShop.sell("Peluche");

		//On vient tester que l'objet sort du magasin
		assertFalse ((listeItems.getItemMap()).containsKey("Peluche"));

		//On vient ensuite tester que le cash du magasin correspond au prix de l'objet vendu
		assert newShop.getCash() == item1.getPrice();
	}

	@Test
	public void test_buy () {

		//On va tester la fonction d'achat
		//On initialise d'abord un stock et un magasin avec cash

		Storage listeItems = new Storage();
		Shop newShop = new Shop() ;
		newShop.setStorage(listeItems);
		newShop.setCash(50);

		//On initialise un objet � acheter
		Item item4= new Item("Montre",04,20,1) ;

		//On teste que l'objet n'est pas en stock
		assertFalse ((listeItems.getItemMap()).containsKey("Montre"));

		// On vient acheter
		newShop.buy(item4);

		//On teste maintenant que l'objet est en stock
		assert ((listeItems.getItemMap()).containsKey("Montre"));

		//On teste que le cash a reduit du prix de l'objet
		assert newShop.getCash() == (50-item4.getPrice());
	}


	@Test
	public void test_isItemAvailable () {

		//On va tester la fonction de pr�sence d'un Item

		//Initialisation item, stock et magasin
		Item patate= new Item("Patate",05,1,1) ;
		Storage stock = new Storage();
		stock.addItem(patate);
		Shop shop3 = new Shop() ;
		shop3.setStorage(stock);

		//On teste que la fonction renvoie true quand l'item existe
		assert (shop3.isItemAvailable("Patate"));

		//On teste qu'elle renvoie faux dans le cas contraire
		assertFalse (shop3.isItemAvailable("Chocolat"));

	}


	@Test
	public void test_getAgeForBook () {

		//On va tester la fonction de r�cup de l'�ge mini pour un livre

		//Initialisation
		Book book1 = new Book("Alice aux pays des merveilles",05,7,1);
		Book book2 = new Book("La Petite Sir�ne",06,7,1);
		book1.setAge(10);
		book2.setAge(8);
		Item not_a_book= new Item("Not a Book",07,1,1) ;
		Storage stock4 = new Storage();
		stock4.addItem(book1);
		stock4.addItem(book2);
		stock4.addItem(not_a_book);
		Shop shop4 = new Shop() ;
		shop4.setStorage(stock4);

		//On v�rifie que pour les livres, on obtient bien l'�ge
		assert shop4.getAgeForBook("Alice aux pays des merveilles") == (book1.getAge());
		assert shop4.getAgeForBook("La Petite Sir�ne") == 8;

		//Et que l'on obtient bien z�ro pour un objet n'�tant pas un livre
		assert shop4.getAgeForBook("Not a Book") == 0;
	}

	@Test
	public void test_getINbtemInStorage () {
		//On va tester qu'on obtient bien le bon nombre d'�l�ments en stock et 0 si pas en stock

		//Initialisation
		Item item8 = new Item("Lunettes de soleil",8,10,5) ; //item qui sera en stock
		Storage stock5 = new Storage();
		stock5.addItem(item8);
		Shop shop5 = new Shop();
		shop5.setStorage(stock5);

		//On teste qu'on obtient bien 5 pour l'item8
		assert (shop5.getINbtemInStorage("Lunettes de soleil")== 5);

		//On teste qu'on obtient bien 0 pour un item hors stock
		assert shop5.getINbtemInStorage("Lego") == 0;


	}

	@Test
	public void test_getQuantityPerConsumable () {

		//On va tester qu'on obtient bien le bon nombre d'�l�ments pour un consommable

		//Initialisation consommable
		Consommable conso1 = new Consommable("Cartons",07,2,10);
		conso1.setQuantity(100);

		//Initialisation stock & Cie
		Storage stock6 = new Storage();
		stock6.addItem(conso1);
		Shop shop6 = new Shop();
		shop6.setStorage(stock6);

		//On teste qu'on obtient bien 100 pour le conso 1
		assert (shop6.getQuantityPerConsumable("Cartons")== 100);

		//On teste qu'on obtient bien 0 pour un conso non en stock
		assert (shop6.getQuantityPerConsumable("Papier")== 0);


	}


	@Test
	public void test_getAllBook () {
		//On va tester la restitution d'une liste de livre
		
		//Cr�ation livres
		Book tome1 = new Book("Tome 1",1,10,1);
		Book tome2 = new Book("Tome 2",2,10,1);
		Book tome3 = new Book("Tome 3",3,10,1);
		Item not_a_book= new Item("Not a Book",4,1,1) ;
		
		
		//Ajout stock et magasin
		Storage stock7 = new Storage();
		stock7.addItem(tome1);
		stock7.addItem(tome2);
		stock7.addItem(tome3);
		stock7.addItem(not_a_book);
		Shop shop7 = new Shop();
		shop7.setStorage(stock7);
		
		//cr�ation de la liste
		List<Book> liste_livres = shop7.getAllBook();
		
		//On v�rifie que la liste contient le livre 1, le 2 et le 3
		assert ((liste_livres).contains(tome1) && (liste_livres).contains(tome2) && (liste_livres).contains(tome3));
	
		//On v�rifie que la liste ne contient pas item4, qui n'est pas un livre
		assertFalse ((liste_livres).contains(not_a_book));

	}


}
