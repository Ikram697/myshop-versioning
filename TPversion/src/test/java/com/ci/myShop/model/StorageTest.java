package com.ci.myShop.model;

import org.junit.Test;

import com.ci.myShop.controller.Storage;

public class StorageTest {
	
	
	@Test
	public void test_addItem () {
		Item it = new Item("test",00,10,1);
		Storage store = new Storage();
		store.addItem(it);
	
		//On teste la m�thode addItem() on vient v�rifier que l'item est contenu dans le storage
		assert((store.getItemMap()).containsKey("test"));
		
	}
	
	@Test
	public void test_getItem () {
		Item it2 = new Item("test2",01,8,1);
		Storage store2 = new Storage();
		store2.addItem(it2);
	
		
		//On teste la m�thode getItem() on vient v�rifier 
		// qu'on obtient bien l'item ajout� 
		// avec son label et qu'on ne l'obtient pas avec un label diff�rent
		
		assert(store2.getItem("test2")==it2);
		assert store2.getItem("toto") != it2;
	}

}
